<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Polymer View Test</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  <!-- Bower Web Components -->
  <script src="bower_components/webcomponentsjs/webcomponents.js" async></script>

  <!-- Paper Card -->
  <link rel="import" href="bower_components/paper-card/paper-card.html">
  <!-- Paper Button -->
  <link rel="import" href="bower_components/paper-button/paper-button.html">

  <!-- Roboto Font -->
  <link rel="import" href="bower_components/font-roboto/roboto.html">
</head>
<style>
.container {
  padding-top: 20%;
  margin: auto;
  height: 200px;
  width: 200px;
}

.goBack {
  padding: 5px;
}
</style>
<body>
  <div class="container">
    <paper-card heading="{{.Website}}">
      <div class="viewPaperCard">
        <p> This is the view page. </p>
        <p> {{.Email}}</p>

        <div class="goBack">
          <p> Let's go back to home. </p>
          <paper-button onclick="goHome()" raised>Home</paper-button>
        </div>
      </div>
    </div>
  </paper-card>
</body>
<script async>
function goHome() {
  window.location = "/";
}
</script>
</html>
