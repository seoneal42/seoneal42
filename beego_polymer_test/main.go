package main

import (
	"github.com/astaxie/beego"
	_ "github.com/seoneal42/beego_polymer_test/routers"
)

func main() {
	beego.SetStaticPath("/bower_components", "bower_components")
	beego.Run()
}
