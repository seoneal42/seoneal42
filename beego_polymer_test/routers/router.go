package routers

import (
	"github.com/astaxie/beego"
	"github.com/seoneal42/beego_polymer_test/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/view", &controllers.ViewController{})
}
