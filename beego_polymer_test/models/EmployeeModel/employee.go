package EmployeeModel

type (
	Employee struct {
		EmpID         string `bson:"emp_id", json:"emp_id"`
		FirstName     string `bson:"first_name", json:"fist_name"`
		LastName      string `bson:"last_name", json:"last_name"`
		Administrator bool   `bson:"is_admin", json:"is_admin"`
	}
)

func (emp *Employee) GetID() string {
	return emp.EmpID
}

func (emp *Employee) GetName() (string, string) {
	return emp.FirstName, emp.LastName
}

func (emp *Employee) IsAdmin() bool {
	return emp.Administrator
}
