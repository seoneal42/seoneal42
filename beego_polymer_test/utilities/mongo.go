package mongo

const (
	//MasterSession direct access to master database
	MasterSession = "master"

	//MonotonicSession provides reads to slave
	MontonicSession = "monotonic"
)

var (
	//Reference to singleton
	singleton mongoManager
)

type (
	//mongoConfiguration contains settings for initialization
	mongoConfiguration struct {
		Host     string
		Database string
		Username string
		Password string
	}
)
