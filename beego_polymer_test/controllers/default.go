package controllers

import (
	"github.com/astaxie/beego"
	Controller "github.com/seoneal42/mongodb_go/Controller"
	"github.com/seoneal42/mongodb_go/Services/Mongo"
)

type MainController struct {
	beego.Controller
}

type ViewController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"

	config := new(mongo.MongoConfig)

	config.Start("../mongodb_go/Configurations/mongo.json")

	defer config.Close()

	c.Data["Pawns"] = Controller.GetPawns(config.Copy())
	c.TplNames = "index.tpl"
}

func (v *ViewController) Get() {
	v.Data["Website"] = "sictech.io"
	v.Data["Email"] = "seoneal42@students.tntech.edu"
	v.TplNames = "view.tpl"
}
