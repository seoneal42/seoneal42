<!DOCTYPE html>
<html>
  <head>
    <title>Login Test Page</title>
    <link rel="stylesheet" href="css/login.css">

    <!-- Bower Web Components -->
    <script src="bower_components/webcomponentsjs/webcomponents.min.js" async></script>

    <!-- Paper Card -->
    <link href="bower_components/paper-card/paper-card.html" rel="import">
  </head>
  <body>
    <div class="loginForm">
        <paper-card>
            <p>{{.error}}</p>
            <form id="form" method="post">
                <input type="text" id="empId" name="empId" value="{{.empId}}" placeholder="Employee ID"/><br>
                <input type="password" id="empId" name="password" placeholder="Password"/><br>
                <input type="submit" value="Login" />
            </form>
        </paper-card>
    </div>
  </body>
</html>
