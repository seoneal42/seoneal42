<!DOCTYPE html>
<html>
    <head>
        <title>{{.title}}</title>

        <!-- Web Components -->
        <script src="/bower_components/webcomponentsjs/webcomponents.js" aysnc></script>

        <!--Paper ToolBar-->
        <link href="/bower_components/paper-toolbar/paper-toolbar.html" rel="import">

        <!--Iron Icon -->
        <link href="/bower_components/iron-icons/iron-icons.html" rel="import">

        <!--Paper Icon Button-->
        <link href="/bower_components/paper-icon-button/paper-icon-button.html" rel="import">

        <!--Paper Input -->
        <link href="/bower_components/paper-input/paper-input.html" rel="import">

        <!--Paper Button-->
        <link href="/bower_components/paper-button/paper-button.html" rel="import">

        <!--Paper CheckBox-->
        <link href="/bower_components/paper-checkbox/paper-checkbox.html" rel="import">

        <!--Paper Card -->
        <link href="/bower_components/paper-card/paper-card.html" rel="import">

        <!--Iron Page -->
        <link href="/bower_components/iron-pages/iron-pages.html" rel="import">

        <!--CSS Layout -->
        <link href="/css/trans_form.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div class="main-menu">
        <paper-toolbar id="main-menu-toolbar">
            <span id="go_back">
                <paper-icon-button icon="arrow-back" title="Go Back!" onclick="onBack()"></paper-icon-button>
            </span>
            <span id="greetings">Hello, {{.first_name}}!</span>
            <span id="logout_icon">
               <paper-icon-button icon="input" title="Logout" onclick="logout()"></paper-icon-button>
               <script>
                  function logout(){
                       location.href="/logout"
                  }
               </script>
            </span>
        </paper-toolbar>
    </div>
    <div class="container">
        <iron-pages selected="0">
            <div id="client">
                <form id="client_form" method="post" action="/app/client" is="iron-form">
                    <div id="first_row">
                        <span id="first_name">
                            <paper-input label="First Name" type="text" name="first_name" value="{{.first_name}}"></paper-input>
                        </span>
                        <span id="middle_i">
                            <paper-input label="Middle I" type="text" name="middle_i" value="{{.middle_i}}"></paper-input>
                        </span>
                        <span id="last_name">
                            <paper-input label="Last Name" type="text" name="last_name" value="{{.last_name}}"></paper-input>
                        </span>
                    </div>
                    <div id="second_row">
                        <span id="address">
                            <paper-input label="Address" type="text" name="addr_name" value="{{.addr_name}}"></paper-input>
                        </span>
                        <span id="city">
                            <paper-input label="City" type="text" name="addr_city" value="{{.addr_city}}"></paper-input>
                        </span>
                        <span id="state">
                            <paper-input label="State" type="text" maxLength="2" name="addr_state" value="{{.addr_state}}"></paper-input>
                        </span>
                        <span id="zip_code">
                            <paper-input label="Zip Code" type="number" maxLength="5" name="addr_zip_code" value="{{.addr_zip_code}}"></paper-input>
                        </span>
                    </div>
                    <div id="third_row">
                        <span id="social_num">
                            <span id="social_num_info">
                                <paper-input type="number" maxLength="3" name="social_num1" value="{{.socialNum1}}"></paper-input>
                                <p>-</p>
                                <paper-input type="number" maxLength="2" name="social_num2" value="{{.social_num2}}"></paper-input>
                                <p>-</p>
                                <paper-input type="number" maxLength="4" name="social_num3" value="{{.social_num3}}"></paper-input>
                                <input type="hidden" name="social_security" value="{{.social_security}}" />
                            </span>
                            <label>Social Security</label>
                        </span>
                        <span id="date_of_birth">
                           <input type="date" name="date_of_birth" value="{{.date_of_birth}}" />
                           <label>Date of Birth</label>
                        </span>
                    </div>
                    <div id="fourth_row">
                        <span id="dri_li">
                            <paper-input label="Driver's License No." type="text" name="driver_li" value="{{.driver_li}}"></paper-input>
                        </span>
                        <span id="dri_li_exp_date">
                            <input type="date" name="dri_li_exp_date" value="{{.dri_li_exp_date}}" />
                            <label>Driver's License Expiration Date</label>
                        </span>
                    </div>
                    <div id="fifth_row">
                        <span id="height">
                            <paper-input type="text" maxLength="4" name="height" value="{{.height}}"></paper-input>
                            <label>Height</label>
                        </span>
                        <span id="weight">
                            <paper-input type="text" maxLength="4" name="weight" value="{{.weight}}"></paper-input>
                            <label>Weight</label>
                        </span>
                        <span id="gender">
                            <paper-input type="text" maxLength="1" name="gender" value="{{.gender}}"></paper-input>
                            <label>Gender</label>
                        </span>
                        <span id="race">
                            <paper-input type="text" maxLength="1" name="race" value="{{.race}}"></paper-input>
                            <label>Race</label>
                        </span>
                    </div>
                    <div id="sixth_row">
                        <span id="submit_buton">
                            <paper-button raised onclick="onSubmit('client')">Submit</paper-button>
                            <input type="hidden" name="client_id" value="{{.client_id}}"/>
                        </span>
                        <span id="clear_form">
                            <paper-button raised onclick="clearForm()">Clear</paper-button>
                        </span>
                        <span id="exit_from">
                            <paper-button raised onclick="onBack()">Exit</paper-button>
                        </span>
                    </div>
                </form>
            </div>
        </iron-pages>
    </div>

    </body>
</html>
