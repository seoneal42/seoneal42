<!DOCTYPE html>
<html>
    <head>

        {{.Header}}
        
        <!-- Paper Card -->
        <link href="bower_components/paper-card/paper-card.html" rel="import">
        
        <!-- Paper Input -->
        <link href="bower_components/paper-input/paper-input.html" rel="import">
        
        <!-- Paper Button -->
        <link href="bower_components/paper-button/paper-button.html" rel="import">
        
        <!-- Paper Checkbox -->
        <link href="bower_components/paper-checkbox/paper-checkbox.html" rel="import">
        
        <!-- Iron Form -->
        <link href="bower_components/iron-form/iron-form.html" rel="import">
        
        <!-- Scripts -->
        <script src="js/signup.js" async></script>
        
        <!-- Styles -->
        <link href="css/signup.css" rel="stylesheet" type="text/css">
    
    </head>
    
    <body>
        {{.Header}}
        
            <div class="container">
                <paper-card>
                    <form is="iron-form" id="signup_form" method="post">
                        <div id="first_row">
                            <span id="first_name"><paper-input name="first_name" value="{{.first_name}}" type="text" maxLength="100"></paper-input></span>
                            <span id="last_name"><paper-input name="last_name" value="{{.last_name}}" type="text" maxLength="100"></paper-input></span>
                        </div>
                        <div id="second_row">
                            <span id="password"><paper-input name="password" type="password" maxLength="30"></paper-input></span>
                            <span id="password_recheck"><paper-input name="password-recheck" type="password" maxLength="30"></paper-input></span>
                        </div>
                        <div id="third_row">
                            <span id="admin"><paper-check name="isAdmin" value="admin"></paper-check></span>
                        </div>
                        <div id="fourth_row">
                            <span id="submit"><paper-button name="submit" onclick="onSubmit()">Submit</paper-button></span>
                            <span id="exit"></span>
                        </div>
                    </form>
                </paper-card>
            </div>
        {{.Footer}}
    </body>
</html>