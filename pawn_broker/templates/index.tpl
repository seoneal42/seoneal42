<!DOCTYPE html>
<html>
  <head>
    <title>Index test </title>

    <!-- Bower Web Components -->
    <script src="bower_components/webcomponentsjs/webcomponents.min.js" async></script>
    <!-- Iron Icon-->
    <link href="bower_components/iron-icons/iron-icons.html" rel="import">
    <!-- Paper Icon Button -->
    <link href="bower_components/paper-icon-button/paper-icon-button.html" rel="import">
    <!-- Paper ToolBar -->
    <link href="bower_components/paper-toolbar/paper-toolbar.html" rel="import">

    <!-- Paper Tabs -->
    <link href="bower_components/paper-tabs/paper-tabs.html" rel="import">
    <!-- Iron Pages -->
    <link href="bower_components/iron-pages/iron-pages.html" rel="import">

    <!-- Paper ListBox -->
    <link href="bower_components/paper-listbox/paper-listbox.html" rel="import">

    <link href="css/index.css" rel="stylesheet">


    <!-- Custom Styles -->
    <style is="custom-style">
        paper-toolbar {
            --paper-toolbar-background: var(--paper-blue-900);
        }
    </style>
  </head>

  <body>
        <div class="main-menu">
              <paper-toolbar id="main-menu-toolbar">
                    <span id="greetings">Hello, {{.first_name}}!</span>
                    <span id="logout_icon">
                          <paper-icon-button icon="input" title="Logout" onclick="logout()"></paper-icon-button>
                          <script>
                                function logout(){
                                      location.href="/logout";
                                }
                          </script>
                    </span>
              </paper-toolbar>
        </div>
        <div class="container">
            <p>{{.error}}</p>
            <span id="paper-tabs">
                <paper-tabs selected="0">
                    <paper-tab>Pawns</paper-tab>
                    <paper-tab>Buys</paper-tab>
                    <paper-tab>Gun Pawns</paper-tab>
                    <paper-tab>Layaways</paper-tab>
                    <paper-tab>Police Reports</paper-tab>
                    <paper-tab>Clients</paper-tab>
                </paper-tabs>
            </span>

            <span id="iron-pages">
                <iron-pages selected="0">
                    <div id="pawn_page">
                        <div><!-- TODO:Put Button Implementation Here -->
                            <paper-icon-button icon="add-circle" onclick="toClientPage()"></paper-icon-button>
                            <script>
                                function toClientPage(){
                                    location.href="/app/client";
                                }
                            </script>
                        </div>
                        <paper-listbox id="pawns_list_box">
                            <paper-item id="paper_item_header">
                                <div>Client ID</div>
                                <div>Pawn No.</div>
                                <div>Date In</div>
                                <div>Date Out</div>
                                <div>Forfeit Date</div>
                                <div>Renewal</div>
                                <div>Paid</div>
                                <div>Forfeit</div>
                                <div>Items</div>
                                <div>Subtotal</div>
                                <div>Total</div>
                            </paper-item>
                            {{range $i, $p := .pawns}}
                            <paper-item id="paper_item_list">
                                <div>{{$p.ClientID}}</div>
                                <div>{{$p.PawnNo}}</div>
                                <div>{{$p.DateIn}}</div>
                                <div>{{$p.DateOut}}</div>
                                <div>{{$p.ForfeitDate}}</div>
                                <div>{{$p.Renewal}}</div>
                                <div><!-- TODO:Put Paid Implementation Here -->{{$p.Paid}}</div>
                                <div><!-- TODO:Put Forfeit Implementation Here -->{{$p.Forfeit}}</div>
                                <div><!-- TODO:Put Item Implemenation Here -->List of Items</div>
                                <div>{{$p.SubTotal}}</div>
                                <div>{{$p.Total}}</div>
                            </paper-item>
                            {{end}}
                        </paper-listbox>
                    </div>
                    <div>Page 2</div>
                    <div>Page 3</div>
                </iron-pages>
            </span>

            <script>
                var pages = document.querySelector('iron-pages');
                var tabs = document.querySelector('paper-tabs');

                tabs.addEventListener('iron-select', function(){
                    pages.selected = tabs.selected;
                });
            </script>
        </div>
  </body>
</html>
