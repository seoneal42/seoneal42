package main

import (
	"encoding/gob"
	"log"
	"runtime"

	"github.com/astaxie/beego"
	"github.com/seoneal42/pawn_broker/models"
	_ "github.com/seoneal42/pawn_broker/routers"
	"github.com/seoneal42/pawn_broker/services/config"
	"github.com/seoneal42/pawn_broker/services/mongo"
	"github.com/seoneal42/pawn_broker/services/session"
)

func init() {
	//Registers all types into gob to be used by session controlling
	gobRegister()

	//Takes control of all available cores
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	configFile, err := config.File("conf/config.json")

	if err != nil {
		log.Fatalf("Could not get configurations: %v\n", err)
	}

	mongoConfig := configFile.DbConfig()
	sessConfig := configFile.SessionConfig()

	session.Configure(sessConfig)

	mongo.Start(mongoConfig)

	beego.SetStaticPath("/bower_components", "static/bower_components")
	beego.SetStaticPath("/css", "static/css")
	beego.SetStaticPath("/js", "static/js")
	beego.SetStaticPath("/html", "static/html")
	beego.Run()
}

func gobRegister() {
	gob.Register(&models.Employee{})
	gob.Register(&models.Client{})
	gob.Register(&models.Pawn{})
	gob.Register(&models.Buy{})
	gob.Register(&models.GunPawn{})
	gob.Register(&models.PoliceReport{})
	gob.Register(&models.Layaway{})
}
