package authentication

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

const saltSize = 16

//GenerateSecurePassword retruns a hashed byte array, does not return error
func GenerateSecurePassword(pass []byte) []byte {
	hashedPassword, err := bcrypt.GenerateFromPassword(pass, saltSize)

	if err != nil {
		log.Println("Could not secure password: ", err)
		return nil
	}

	return hashedPassword
}

//Validate returns boolean value, returns true if password matches hash
func Validate(pass []byte, hashedPass []byte) bool {
	err := bcrypt.CompareHashAndPassword(hashedPass, pass)

	if err != nil {
		log.Println("func ValidatePassword(): Password accepted.")
		return true
	} else {
		log.Println("func ValidatePassword(): Password invalid.")
		return false
	}
}
