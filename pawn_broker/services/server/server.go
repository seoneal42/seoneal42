package server

import (
      "fmt"
      "log"
      "net/http"
      "time"
      
      "github.com/seoneal42/pawn_broker/services/config"
)

//Run starts the HTTP and/or HTTPS listener
func Run(httpHandlers http.Handler, httpsHandlers http.Handler, s config.Server) {
      if s.UseHTTP && s.UseHTTPS {
            go func() {
                  startHTTPS(httpsHandlers, s)
            }()
            
            startHTTP(httpHandlers, s)
      }else if s.UseHTTP {
            startHTTP(httpHandlers, s)
      }else if s.UseHTTPS {
            startHTTPS(httpsHandlers, s)
      }else {
            log.Println("Config file does not specify a listener to start")
      }
}

// startHTTP starts the HTTP listener
func startHTTP(handlers http.Handler, s config.Server) {
	fmt.Println(time.Now().Format("2006-01-02 03:04:05 PM"), "Running HTTP "+httpAddress(s))

	// Start the HTTP listener
	log.Fatal(http.ListenAndServe(httpAddress(s), handlers))
}

// startHTTPs starts the HTTPS listener
func startHTTPS(handlers http.Handler, s config.Server) {
	fmt.Println(time.Now().Format("2006-01-02 03:04:05 PM"), "Running HTTPS "+httpsAddress(s))

	// Start the HTTPS listener
	log.Fatal(http.ListenAndServeTLS(httpsAddress(s), s.CertFile, s.KeyFile, handlers))
}

//httpAddress returns the HTTP address
func httpAddress(s config.Server) string {
      return s.Hostname + ":" + fmt.Sprintf("%d", s.HTTPPort)
}

//httpsAddress returns the HTTPS address
func httpsAddress (s config.Server) string {
      return s.Hostname + ":" + fmt.Sprintf("%d", s.HTTPSPort)
}