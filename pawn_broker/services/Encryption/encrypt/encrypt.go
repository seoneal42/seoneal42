package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
	"log"
)

var key = make([]byte, 32)

//EncryptData encrypts the information given to the fuction and returns
//the encrypted value as a byte array.
func EncryptData(info []byte) []byte {
	block, err := aes.NewCipher(key)

	if err != nil {
		log.Printf("func EncryptData(): Could not get cipher: %v\n", err)
		return nil
	}

	b := base64.StdEncoding.EncodeToString(info)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		log.Println(err)
		return nil
	}

	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext
}
