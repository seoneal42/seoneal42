package decrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"log"
)

var key = make([]byte, 32)

//DecryptData decrypts the encrypted data given
//and returns it as a byte array
func DecryptData(encryptedText []byte) []byte {
	block, err := aes.NewCipher(key)

	if err != nil {
		log.Printf("func DecryptData(): Could not create new cipher: %v\n", err)
		return nil
	}

	if len(encryptedText) < aes.BlockSize {
		log.Println("func DecryptData(): Cipher text too short")
		return nil
	}

	iv := encryptedText[:aes.BlockSize]
	text := encryptedText[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)

	data, err := base64.StdEncoding.DecodeString(string(text))

	if err != nil {
		log.Printf("func DecryptData(): Could not decode info: %v\n", err)
		return nil
	}

	return data
}
