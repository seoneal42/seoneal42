package session

import (
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/seoneal42/pawn_broker/services/config"
	"log"
)

var (
	//Store is the cookie store
	Store *sessions.CookieStore
	//Name is the session name
	Name string
)

//Configure the session cookie store
func Configure(s config.Sessions) {
	Store = sessions.NewCookieStore([]byte(s.SecretKey))
	Store.Options = &sessions.Options{
		HttpOnly: s.Option.HttpOnly,
		Domain: s.Option.Domain,
		Secure: s.Option.Secure,
		MaxAge: s.Option.MaxAge,
		Path : s.Option.Path,
	}
	Name = s.Name
	log.Println(s.Name)
}

//Instance returns a new session, never returns an error
func Instance(r *http.Request) *sessions.Session {
	session, _ := Store.Get(r, Name)
	return session
}

//Empty deletes all the current session values
func Empty(sess *sessions.Session) {
	//Clear out all stored values in the cookie
	for k := range sess.Values {
		delete(sess.Values, k)
	}
}
