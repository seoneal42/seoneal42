package mongo

import (
	"errors"
	"fmt"
	"time"

	config "github.com/seoneal42/pawn_broker/services/config"
	"gopkg.in/mgo.v2"

	"log"
)

type (
	Settings struct {
		MongoDBHosts []string
		AuthDatabase string
		AuthUserName string
		AuthPassword string
	}

	Config struct {
		settings *Settings
		dialInfo *mgo.DialInfo
		sess     *mgo.Session
	}
)

var (
	staticConfig *Config
)

func getSettings(dbConfig config.Database) {
	fmt.Println(dbConfig)
	staticConfig.settings.MongoDBHosts = dbConfig.MongoDBHosts
	staticConfig.settings.AuthDatabase = dbConfig.AuthDatabase
	staticConfig.settings.AuthUserName = dbConfig.AuthUserName
	staticConfig.settings.AuthPassword = dbConfig.AuthPassword

	info := &mgo.DialInfo{
		Addrs:    staticConfig.settings.MongoDBHosts,
		Username: staticConfig.settings.AuthUserName,
		Password: staticConfig.settings.AuthPassword,
		Timeout:  60 * time.Second,
		Database: staticConfig.settings.AuthDatabase,
	}

	staticConfig.dialInfo = info
}

func getSession() {
	log.Println("func getSession(): Making connection ...")

	s, err := mgo.DialWithInfo(staticConfig.dialInfo)

	if err != nil {
		log.Fatalf("func getSession(): Could not connect: %v\n", err)
	}

	for i := 0; i < len(staticConfig.dialInfo.Addrs); i++ {
		log.Println("func getSession(): ", staticConfig.dialInfo.Addrs[i])
	}

	s.SetMode(mgo.Monotonic, true)
	staticConfig.sess = s
}

func Start(dbConfig config.Database) {
	staticConfig = new(Config)

	staticConfig.dialInfo = new(mgo.DialInfo)
	staticConfig.sess = new(mgo.Session)
	staticConfig.settings = new(Settings)

	log.Println("Starting connection to database...")

	getSettings(dbConfig)
	getSession()
}

func Copy() *mgo.Session {
	dbNames, _ := staticConfig.sess.DatabaseNames()

	if len(dbNames) == 0 {
		log.Println("func Copy(): No connection to database.")
		return nil
	}

	return staticConfig.sess.Copy()
}

func Database() string {
	return staticConfig.settings.AuthDatabase
}

func GetItemBySearch(collectionName string, id interface{}, item interface{}) (error) {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Find(id).One(&item)

		if err != nil {
			log.Println("Could not query collection: ", err)
			return err
		}

		return nil
	}

	err := errors.New("No DB to connect to.")

	return err
}

func GetItems(collectionName string, items interface{}) (error) {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Find(nil).All(items)

		if err != nil {
			log.Println("Could not query items: ", err)
			return err
		}

		return nil
	}

	err := errors.New("No DB to connect to.")
	return err
}

func GetItemsBySearch(collectionName string, search interface{}, items interface{}) (error) {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		var items []interface{}

		err := col.Find(search).All(&items)

		if err != nil {
			log.Println("Could not query: ", err)
			return err
		}
		return nil
	}
	err := errors.New("No DB to connect to.")
	return err
}

func GetItemsByDate(collectionName string, dateRange interface{}, items interface{}) (error) {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Find(dateRange).All(&items)

		if err != nil {
			log.Println("Could not query items: ", err)
			return err
		}

		return nil
	}

	err := errors.New("No DB to connect to.")
	return err
}

func InsertItem(collectionName string, item interface{}) error {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Insert(item)

		if err != nil {
			log.Println("Could not insert item: ", err)
			return err
		}

		return nil
	}

	err := errors.New("No DB to connect to.")
	return err
}

func InsertItems(collectionName string, items []interface{}) error {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Insert(items)

		if err != nil {
			log.Println("Could not insert items: ", err)
			return err
		}

		return nil
	}

	err := errors.New("No DB to connect to.")
	return err
}

func UpdateItem(collectionName string, id interface{}, item interface{}) error {
	sess := Copy()

	if sess != nil {
		defer sess.Close()

		col := sess.DB(staticConfig.settings.AuthDatabase).C(collectionName)

		err := col.Update(id, item)

		if err != nil {
			log.Println("Could not update: ", err)
			return err
		}
		return nil
	}
	err := errors.New("No DB to connect to.")
	return err
}
