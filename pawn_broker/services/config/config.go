package config

import (
	"encoding/json"
	"log"

	"fmt"

	"io/ioutil"
      "github.com/gorilla/sessions"
)

type Database struct {
	MongoDBHosts []string `json:"MongoDBHosts"`
	AuthDatabase string `json:"AuthDatabase"`
	AuthUserName string `json:"AuthUserName"`
	AuthPassword string `json:"AuthPassword"`
}

type Options struct {
	Path     string `json:"Path"`
	Domain   string `json:"Domain"`
	MaxAge   int    `json:"MaxAge"`
	Secure   bool   `json:"Secure"`
	HttpOnly bool   `json:"HttpOnly"`
}

type Sessions struct {
	Name      string  `json:"Name"`
	SecretKey string  `json:"SecretKey"`
	Option    sessions.Options `json:"Options"`
}

type Template struct {
      Root string `json:"Root"`
      Children []string `json:"Children"`
}

type View struct {
      BaseURI string    `json:"BaseURI"`
      Extension string  `json:"Extension"`
      Folder string     `json:"Folder"`
      Name string `json:"blank"`
      Caching bool `json:"Caching"`
}
type Server struct {
	Hostname  string `json:"Hostname"`  // Server name
	UseHTTP   bool   `json:"UseHTTP"`   // Listen on HTTP
	UseHTTPS  bool   `json:"UseHTTPS"`  // Listen on HTTPS
	HTTPPort  int    `json:"HTTPPort"`  // HTTP port
	HTTPSPort int    `json:"HTTPSPort"` // HTTPS port
	CertFile  string `json:"CertFile"`  // HTTPS certificate
	KeyFile   string `json:"KeyFile"`   // HTTPS private key
}

type JsonObject struct {
	Db   Database `json:"Database"`
	Opt  Options  `json:"Options"`
	Sess Sessions `json:"Sessions"`
	Serv Server   `json:"Server"`
      Tmpl Template `json"Template"`
      V     View `json:"View"`
}

func File(fileName string) (*JsonObject, error) {
	file, err := ioutil.ReadFile(fileName)

	if err != nil {
		log.Printf("func GetConfig(): %v", err)
		return nil, err
	}

	newJO := new(JsonObject)
	getSettings(file, newJO)

	return newJO, nil
}

func getSettings(file []byte, jo *JsonObject) {
	var newJs JsonObject
	err := json.Unmarshal(file, &newJs)

	if err != nil {
		log.Printf("func getSettings(): %v\n", err)
	} else {
		fmt.Printf("Result:\n %v\n", newJs)
	}

	jo.Db = newJs.Db
	jo.Opt = newJs.Opt
	jo.Sess = newJs.Sess
	jo.Serv = newJs.Serv
}

func (jo *JsonObject) DbConfig() Database {
	return jo.Db
}

func (jo *JsonObject) ServerConfig() Server {
	return jo.Serv
}

func (jo *JsonObject) SessionConfig() Sessions {
	return jo.Sess
}

func (jo *JsonObject) OptionsConfig() Options {
	return jo.Opt
}

func (jo *JsonObject) TemplateConfig() Template {
      return jo.Tmpl
}

func (jo *JsonObject) ViewConfig() View {
      return jo.V
}