<div class="main-menu">
      <paper-toolbar>
            <span id="greetings">Hello, {{.first_name}}!</span>
            <paper-icon-button id="logout" icon="icons:input" alt="Logout" onclick="logout()"></paper-icon-button>
            <script>
                  function logout(){
                        location.href="/logout"
                  }
            </script>
      </paper-toolbar>
</div>
