package routers

import (
	"github.com/astaxie/beego"
	"github.com/seoneal42/pawn_broker/controllers"
	"github.com/seoneal42/pawn_broker/controllers/app"
)

func init() {
	beego.Router("/", &controllers.IndexController{})
	beego.Router("/login", &controllers.LoginController{})
	beego.Router("/logout", &controllers.LogoutController{})
	beego.Router("/app/client", &app.AppClientController{})
}
