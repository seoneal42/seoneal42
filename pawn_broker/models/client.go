package models

import (
	"time"

	"github.com/seoneal42/pawn_broker/services/Encryption/decrypt"
	"github.com/seoneal42/pawn_broker/services/Encryption/encrypt"
	"github.com/seoneal42/pawn_broker/services/mongo"

	"gopkg.in/mgo.v2/bson"
)

//Client structure
type Client struct {
	ClientID        string    `bson:"client_id",json:"client_id",form:"client_id"`
	FirstName       string    `bson:"first_name",json:"first_name",form:"first_name"`
	LastName        string    `bson:"last_name",json:"last_name",form:"last_name"`
	MiddleI         string    `bson:"middle_i",json:"middle_i",form:"middle_i"`
	AddrName        string    `bson:"addr_name",json:"addr_name",form:"addr_name"`
	AddrCity        string    `bson:"addr_city",json:"addr_city",form:"addr_city"`
	AddrState       string    `bson:"addr_state",json:"addr_state",form:"addr_state"`
	AddrZipCode     string    `bson:"addr_zipcode",json:"addr_zipcode",form:"addr_zip_code"`
	DateOfBirth     time.Time `bson:"date_of_birth",json:"date_of_birth",form:"date_of_birth"`
	DriverLi        string    `bson:"driver_li",json:"driver_li",form:"driver_li"`
	DriverLiExpDate time.Time `bson:"driver_li_exp_date",json:"driver_li_exp_date",form:"driver_li_exp_date"`
	SocialSecurity  string    `bson:"social_security",json:"social_security",form:"social_security"`
	Race            string    `bson:"race",json:"race",form:"race"`
	Gender          string    `bson:"gender",json:"gender",form:"gender"`
	Height          string    `bson:"height",json:"height",form:"height"`
	Weight          int       `bson:"weight",json:"weight",form:"weight"`
}

//EncryptInfo  encrypts client's information
func (c *Client) EncryptInfo() {
	c.DriverLi = string(encrypt.EncryptData([]byte(c.DriverLi)))
	c.SocialSecurity = string(encrypt.EncryptData([]byte(c.SocialSecurity)))
	c.AddrName = string(encrypt.EncryptData([]byte(c.AddrName)))
}

//DecryptInfo decrypts client's information and should only be used
//once encryption has been done
func (c *Client) DecryptInfo() {
	c.DriverLi = string(decrypt.DecryptData([]byte(c.DriverLi)))
	c.SocialSecurity = string(decrypt.DecryptData([]byte(c.SocialSecurity)))
	c.AddrName = string(decrypt.DecryptData([]byte(c.AddrName)))
}

//ClientByID gets a client from the database based on id given
func ClientByID(clientId string) (Client, error) {
	queryCmd := bson.M{"client_id": clientId}

	var client = &Client{}

	err := mongo.GetItemBySearch("employees", queryCmd, *client)

	return *client, err
}

//ClientAll pulls all clients from database
func ClientAll() ([]Client, error) {
	var clientList []Client

	err := mongo.GetItems("clients", clientList)

	return clientList, err
}

//ClientByDateOfBirth gets a list of clients based on a starting date point and ending date point
func ClientByDateOfBirth(startDate, endDate time.Time) ([]Client, error) {
	queryCmd := bson.M{"date_of_birth": bson.M{"start_date": startDate, "end_date": endDate}}

	var clientList []Client

	err := mongo.GetItemsByDate("clients", queryCmd, clientList)

	return clientList, err
}

//ClientSearch is the generic searching function
func ClientSearch(option, search string) ([]Client, error) {
	queryCmd := bson.M{option: search}

	var clientList []Client

	err := mongo.GetItemsBySearch("clients", queryCmd, clientList)

	return clientList, err
}

func InsertClient(newClient Client) error {
	return mongo.InsertItem("clients", newClient)
}

func UpdateClient(oldClient Client) error {
	return mongo.UpdateItem("clients", oldClient.ClientID, oldClient)
}
