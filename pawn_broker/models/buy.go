package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/seoneal42/pawn_broker/services/mongo"
)

type Buy struct {
	ClientID string    `bson:"client_id"`
	BuyNo    string    `bson:"buy_no"`
	DateIn   time.Time `bson:"date_in"`
	DateOut  time.Time `bson:"date_out"`
	Items    Item      `bson:"items"`
	Total    float32   `bson:"total"`
}

func BuyByID(buyNo string) (Buy, error) {
	queryCmd := bson.M{"buy_no" : buyNo}

	var buy = &Buy{}

	err := mongo.GetItemBySearch("buys", queryCmd, *buy)

	return *buy, err
}

func BuyAll() ([]Buy, error) {
	var buyList []Buy

	err := mongo.GetItems("buys", buyList)

	return buyList, err
}

func BuyByDateIn(startDate, endDate time.Time) ([]Buy, error) {
	queryCmd := bson.M{"date_in": bson.M{"start_date": startDate, "end_date": endDate}}

	var buyList []Buy

	err := mongo.GetItemsByDate("buys", queryCmd, buyList)

	return buyList, err
}

func BuySearch(option, search string) ([]Buy, error) {
	queryCmd := bson.M{option : search}

	var buyList []Buy

	err := mongo.GetItemsBySearch("buys", queryCmd, buyList)

	return buyList, err
}

func InsertBuy(newBuy Buy) error {
	return mongo.InsertItem("buys", newBuy)
}

func UpdateBuy(oldBuy Buy) error {
	return mongo.UpdateItem("buys", oldBuy.BuyNo, oldBuy)
}
