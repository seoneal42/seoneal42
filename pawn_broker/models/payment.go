package models

import (
	"time"
)

type Payment struct {
	PaymentID int       `bson:"payment_id"`
	PaidDate  time.Time `bson:"paid_date"`
	Amount    float32   `bson:"amount"`
}
