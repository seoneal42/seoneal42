package models

type Item struct {
	Id       int     `bson:"item_id"`
	Desc     string  `bson:"item_desc"`
	Make     string  `bson:"item_make"`
	Model    string  `bson:"item_model"`
	SerialNo string  `bson:"item_serialNo"`
	Price    float32 `bson:"item_price"`
}
