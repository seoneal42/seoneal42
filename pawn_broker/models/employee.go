package models

import (
	auth "github.com/seoneal42/pawn_broker/services/Auth"
	"github.com/seoneal42/pawn_broker/services/mongo"
	"gopkg.in/mgo.v2/bson"

	"errors"
	"log"
)

type Employee struct {
	EmpId     string `bson:"emp_id",json:"emp_id"`
	FirstName string `bson:"first_name",json:"first_name"`
	LastName  string `bson:"last_name",json:"last_name"`
	Password  []byte `bson:"secure_password"`
	Admin     bool   `bson:"is_admin",json:"is_admin"`
}

func (emp *Employee) securePassword(pass []byte) error {
	if len(pass) == 0 {
		log.Println("func SecurePassword(): Password passed by value is too short.")

		err := errors.New("Password can not be empty. Try again.")

		return err
	}

	securePass := auth.GenerateSecurePassword(pass)

	emp.Password = securePass
	return nil
}

func (emp *Employee) PasswordCheck(pass []byte) bool {
	if len(pass) == 0 {
		log.Println("func PasswordCheck(): Password passed by value is too short.")
		return false
	}

	if passErr := auth.Validate(pass, emp.Password); !passErr {
		log.Printf("Passowrd is not correct with employee: %v\n", passErr)

		return false
	}

	log.Printf("%v logged in successfully.\n", emp.EmpId)
	return true
}

func (emp *Employee) SetPassword(pass []byte) error {
	return emp.securePassword(pass)
}

func EmployeeById(id string) (Employee, error) {
	queryCmd := bson.M{"emp_id": id}

	var emp = &Employee{}

	err := mongo.GetItemBySearch("employees", queryCmd, *emp)

	return *emp, err
}

func EmployeeAll() ([]Employee, error) {
	var empList []Employee

	err := mongo.GetItems("employees", empList)

	return empList, err
}

func InsertEmployee(newEmp Employee, isAdmin bool) error {
	if !isAdmin {
		err := errors.New("No authorized for this action.")
		return err
	}

	return mongo.InsertItem("employees", newEmp)
}

func UpdateEmployee(oldEmp Employee, isAdmin bool) error {
	if !isAdmin {
		err := errors.New("Not authorized for this operation.")
		log.Println("Unauthorized user attempted changing employee information.")
		return err
	}
	queryCmd := bson.M{"emp_id": oldEmp.EmpId}

	return mongo.UpdateItem("employees", queryCmd, oldEmp)
}
