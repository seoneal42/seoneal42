package models

import (
	"time"

	"github.com/seoneal42/pawn_broker/services/mongo"
	"gopkg.in/mgo.v2/bson"
)

type PoliceReport struct {
	PoliceReportID int       `bson:"police_report_id"`
	CreatedDate    time.Time `bson:"created_date"`
	Pawns          []Pawn    `bson:"pawns"`
	Buys           []Buy     `bson:"buys"`
	GunPawns       []GunPawn `bson:"gun_pawns"`
}

func PoliceReportById(prId int) (PoliceReport, error) {
	queryCmd := bson.M{"police_report_id" : prId}

	var policereport = &PoliceReport{}

	err := mongo.GetItemBySearch("policereports", queryCmd, *policereport)

	return *policereport, err
}

func PoliceReportAll() ([]PoliceReport, error) {
	var policereportList []PoliceReport

	err := mongo.GetItems("policereports", policereportList)

	return policereportList, err
}

func PoliceReportByDateCreated(startDate, endDate time.Time) ([]PoliceReport, error) {
	queryCmd := bson.M{"created_date": bson.M{"start_date": startDate, "end_date": endDate}}

	var policereportList []PoliceReport

	err := mongo.GetItemsByDate("policereports", queryCmd, policereportList)

	return policereportList, err
}

func PoliceReportSearch(option, search string) ([]PoliceReport, error) {
	queryCmd := bson.M{option : search}

	var policereportList []PoliceReport

	err := mongo.GetItemsBySearch("policereports", queryCmd, policereportList)

	return policereportList, err
}

func InsertPoliceReport(newPoliceReport PoliceReport) error {
	return mongo.InsertItem("policereports", newPoliceReport)
}

func UpdatePoliceReport(oldPoliceReport PoliceReport) error {
	return mongo.UpdateItem("policereports", oldPoliceReport.PoliceReportID, oldPoliceReport)
}
