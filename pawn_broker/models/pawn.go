package models

import (
	"time"

	"github.com/seoneal42/pawn_broker/services/mongo"

	"gopkg.in/mgo.v2/bson"
)

type Pawn struct {
	ClientID    string    `bson:"client_id"`
	PawnNo      int       `bson:"pawn_no"`
	DateIn      time.Time `bson:"date_in"`
	DateOut     time.Time `bson:"date_out"`
	ForfeitDate time.Time `bson:"forfeit_date"`
	Renewal     int       `bson:"renewal"`
	Paid        bool      `bson:"paid"`
	Forfeit     bool      `bson:"forfeit"`
	Items       []Item    `bson:"items"`
	SubTotal    float32   `bson:"sub_total"`
	PawnFee     float32   `bson:"pawn_fee"`
	Total       float32   `bson:"total"`
}

func PawnByID(pawnNo int) (Pawn, error) {
	queryCmd := bson.M{"pawn_no" : pawnNo}

	var pawn = &Pawn{}

	err := mongo.GetItemBySearch("pawns", queryCmd, *pawn)

	return *pawn, err
}

func PawnAll() ([]Pawn, error) {
	var pawnList []Pawn

	err := mongo.GetItems("pawns", &pawnList)

	return pawnList, err
}

func PawnByDateIn(startDate, endDate time.Time) ([]Pawn, error) {
	queryCmd := bson.M{"date_in": bson.M{"start_date": startDate, "end_date": endDate}}

	var pawnList []Pawn

	err := mongo.GetItemsByDate("pawns", queryCmd, pawnList)

	return pawnList, err
}

func PawnSearch(option, search string) ([]Pawn, error) {
	queryCmd := bson.M{option : search}

	var pawnList []Pawn

	err := mongo.GetItemsBySearch("pawns", queryCmd, pawnList)

	return pawnList, err
}

func InsertPawn(newPawn Pawn) error {
	return mongo.InsertItem("pawns", newPawn)
}

func UpdatePawn(oldPawn Pawn) error {
	return mongo.UpdateItem("pawns", oldPawn.PawnNo, oldPawn)
}
