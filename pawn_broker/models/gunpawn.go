package models

import (
	"time"

	"github.com/seoneal42/pawn_broker/services/mongo"

	"gopkg.in/mgo.v2/bson"
)

type GunPawn struct {
	ClientID    string    `bson:"client_id"`
	GunPawnNo   int       `bson:"gun_pawn_no"`
	DateIn      time.Time `bson:"date_in"`
	DateOut     time.Time `bson:"date_out"`
	ForfeitDate time.Time `bson:"forfeit_date"`
	Renewal     int       `bson:"renewal"`
	Paid        bool      `bson:"paid"`
	Forfeit     bool      `bson:"forfeit"`
	Items       Item      `bson:"items"`
	SubTotal    float32   `bson:"sub_total"`
	PawnFee     float32   `bson:"pawn_fee"`
	TbiFee      float32   `bson:"tbi_fee"`
	ReturnTotal float32   `bson:"return_total"`
}

func GunPawnById(gunPawnNo int) (GunPawn, error) {
	queryCmd := bson.M{"gun_pawn_no" : gunPawnNo}

	var gunPawn = &GunPawn{}

	err := mongo.GetItemBySearch("buys", queryCmd, *gunPawn)

	return *gunPawn, err
}

func GunPawnByDateIn(startDate, endDate time.Time) ([]GunPawn, error) {
	queryCmd := bson.M{"date_in": bson.M{"start_date": startDate, "end_date": endDate}}

	var gunPawnList []GunPawn

	err := mongo.GetItemsByDate("gunpawns", queryCmd, gunPawnList)

	return gunPawnList, err
}

func GunPawnAll() ([]GunPawn, error) {
	var gunPawnList []GunPawn

	err := mongo.GetItems("gunpawns", gunPawnList)

	return gunPawnList, err
}

func GunPawnSearch(option, search string) ([]GunPawn, error) {
	queryCmd := bson.M{option : search}

	var gunPawnList []GunPawn

	err := mongo.GetItemsBySearch("gunpawns", queryCmd, gunPawnList)

	return gunPawnList, err
}

func InsertGunPawn(newGunPawn GunPawn) error {
	return mongo.InsertItem("gunpawns", newGunPawn)
}

func UpdateGunPawn(oldGunPawn GunPawn) error {
	return mongo.UpdateItem("gunpawns", oldGunPawn.GunPawnNo, oldGunPawn)
}
