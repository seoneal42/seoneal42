package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/seoneal42/pawn_broker/services/mongo"
)

type Layaway struct {
	ClientID     string      `bson:"clientID"`
	LayawayNo    int         `bson:"layawayNo"`
	DateIn       time.Time   `bson:"date_in"`
	DateOut      time.Time   `bson:"date_out" `
	PaymentDates []time.Time `bson:"payment_dates"`
	Payments     []Payment   `bson:"payments"`
	Items        Item        `bson:"items"`
	SubTotal     float32     `bson:"sub_total"`
	DownPayment  float32     `bson:"down_payment"`
	CostOfGoods  float32     `bson:"cost_of_goods"`
	Total        float32     `bson:"total"`
}

func LayawayById(layawayNo int) (Layaway, error) {
	queryCmd := bson.M{"layawayNo" : layawayNo}

	var layaway = &Layaway{}

	err := mongo.GetItemBySearch("layaways", queryCmd, *layaway)

	return *layaway, err
}

func LayawayAll() ([]Layaway, error) {
	var layawayList []Layaway

	err := mongo.GetItems("layaways", layawayList)

	return layawayList, err
}

func LayawayByDateIn(startDate, endDate time.Time) ([]Layaway, error) {
	queryCmd := bson.M{"date_in": bson.M{"start_date": startDate, "end_date": endDate}}

	var layawayList []Layaway

	err := mongo.GetItemsByDate("layaways", queryCmd, layawayList)

	return layawayList, err
}

func LayawaySearch(option, search string) ([]Layaway, error) {
	queryCmd := bson.M{option : search}

	var layawayList []Layaway

	err := mongo.GetItemsBySearch("layaways", queryCmd, layawayList)

	return layawayList, err
}

func InsertLayaway(newLayaway Layaway) error {
	return mongo.InsertItem("layaways", newLayaway)
}

func UpdateLayaway(oldLayaway Layaway) error {
	return mongo.UpdateItem("layaways", oldLayaway.LayawayNo, oldLayaway)
}
