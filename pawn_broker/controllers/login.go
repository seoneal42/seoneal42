package controllers

import (
	"log"
	"net/http"

	"github.com/astaxie/beego"
	"github.com/seoneal42/pawn_broker/models"
	"github.com/seoneal42/pawn_broker/services/session"

	"github.com/gorilla/sessions"
)

type LoginController struct {
	beego.Controller
	sess *sessions.Session
}

type loginInput struct {
	empId    string `form:"empId"`
	password string `form:"password"`
}

func (this *LoginController) Prepare() {
	//TODO:Get session
	this.sess = session.Instance(this.Ctx.Request)

	if this.sess != nil && this.sess.Values["empId"] != nil {
		this.sess.Values["error"] = "Already logged in."
		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		this.Ctx.Redirect(http.StatusFound, "/")
	}
}

func (this *LoginController) Get() {
	//TODO:pulls any form data from session
	this.Data["error"] = this.sess.Values["error"]
	this.Data["empId"] = this.sess.Values["empId"]

	this.TplName = "login.tpl"
}

func (this *LoginController) Post() {
	empId := this.GetString("empId")
	log.Println("Employee: ", empId)

	emp, err := models.EmployeeById(empId)

	if err != nil {
		this.sess.Values["error"] = "Sorry, there is no employee by that id."
		//TODO:Setup error sessioning
		this.Get()
		return
	}

	if emp.PasswordCheck([]byte(this.GetString("password"))) {
		//TODO:Sessionging
		session.Empty(this.sess)

		this.sess.Values["first_name"] = emp.FirstName
		this.sess.Values["empId"] = emp.EmpId
		this.sess.Values["error"] = "Successfully logged in."

		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		this.Redirect("/", http.StatusFound)
	} else {
		this.sess.Values["error"] = "Invalid password. Please try again."
		this.sess.Values["empId"] = empId
		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		//TODO:Setup error sessioning
		this.Ctx.Redirect(http.StatusFound, "/login")
	}
}
