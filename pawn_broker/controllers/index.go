package controllers

import (
	"github.com/astaxie/beego"
	"github.com/seoneal42/pawn_broker/services/session"
	"github.com/seoneal42/pawn_broker/models"

	"github.com/gorilla/sessions"

	"net/http"
)

type IndexController struct {
	beego.Controller
	sess *sessions.Session
}

func (this *IndexController) Prepare() {
	this.sess = session.Instance(this.Ctx.Request)
}

func (this *IndexController) Get() {
	if this.sess != nil && this.sess.Values["empId"] != nil {
		this.Data["empId"] = this.sess.Values["empId"]
		this.Data["first_name"] = this.sess.Values["first_name"]

		pawnList, err := models.PawnAll()

		if err != nil {
			this.Data["pawn_error"] = "Could not load the pawns from DB."
		} else {
			this.Data["pawns"] = pawnList
		}
		
		this.TplName = "index.tpl"
		return
	} else if this.sess.Values["empId"] == nil {
		this.sess.Values["error"] = "Not authorized to be here."

		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		this.Ctx.Redirect(http.StatusFound, "/login")
		return
	}

	this.Ctx.Redirect(http.StatusFound, "/login")
}
