package app

import (
	"github.com/astaxie/beego"

	"github.com/seoneal42/pawn_broker/services/session"

	"github.com/gorilla/sessions"

	"net/http"
)

type AppClientController struct {
	beego.Controller;
	sess *sessions.Session;
}

func (this *AppClientController) Prepare(){
	this.sess = session.Instance(this.Ctx.Request)
}

func (this *AppClientController) Get(){
	if this.sess != nil && this.sess.Values["empId"] != nil{
		this.Data["empId"] = this.sess.Values["empId"]
		this.Data["first_name"] = this.sess.Values["first_name"]
		this.Data["form_type"] = "pawn"
		this.Data["error"] = this.sess.Values["error"]

		this.TplName = "trans_form.tpl"
		return
	} else if this.sess.Values["empId"] == nil {
		this.sess.Values["error"] = "Not authorized to be here."

		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		this.Ctx.Redirect(http.StatusFound, "/login")
		return
	}

	this.Ctx.Redirect(http.StatusFound, "/login")
}

func (this *AppClientController) Post(){
	if this.sess != nil && this.sess.Values["empId"] != nil {
		client := &models.Client{}
		
		err := this.ParseForm(&client)
		
		if err != nil {
			this.sess.Values["error"] = "Could not gather data, please try again."
			this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
			this.Ctx.Redirect(http.StatusFound, "/app/client")
			return
		}
		
		log.Println("Adding ", ClientID, " to database.")
		
		//Encrypt client information
		client.EncryptInfo()
		
		err = models.InsertClient(client)
		
		if err != nil {
			this.sess.Values["error"] = "Issue with server. Please contact administrator."
			this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
			
			this.Ctx.Redirect(http.StatusFound, "/app/client")
			return
		}
		
		this.sess.Values["error"] = "Successfuly added client."
		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		
		this.Ctx.Redirect(http.StatusFound, "/app/client")
		
		return
		
	}else if this.sess.Values["empId"] == nil{
		this.sess.Values["error"] = "Not authorized for this action."
		
		this.sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
		this.Ctx.Redirect(http.StatusFound, "/login")
		return
	}
	
	this.Ctx.Redirect(http.StatusFound, "/")
}