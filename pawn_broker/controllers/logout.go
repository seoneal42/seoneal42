package controllers

import (
	"github.com/seoneal42/pawn_broker/services/session"
	"github.com/astaxie/beego"

	"net/http"
)

type LogoutController struct {
	beego.Controller
}

func (this *LogoutController) Prepare() {
	//Get session
	sess := session.Instance(this.Ctx.Request)

	//If user is authenticated
	if sess.Values["empId"] != nil {
		session.Empty(sess)
		sess.Values["error"] = "Goodbye!"
		sess.Save(this.Ctx.Request, this.Ctx.ResponseWriter)
	}

	this.Ctx.Redirect(http.StatusFound, "/")
}