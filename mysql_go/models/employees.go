package models

type Employee struct {
	ID        string
	FirstName string
	LastName  string
	Password  string
}

var EmployeeList *Employee
