package controllers

import "gopkg.in/mgo.v2"

func Connect() (bool, int64) {
	uri := "mongodb://192.168.1.105/test"

	sess, err := mgo.Dial(uri)

	if err != nil {
		defer sess.Close()
		return false, 0
	}
	defer sess.Close()
	return true, 1

}
