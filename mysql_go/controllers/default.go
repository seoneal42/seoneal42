package controllers

import (
	"strconv"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

type MysqlController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplNames = "index.tpl"
}

func (m *MysqlController) Get() {
	con, valInt := Connect()

	var connVar string

	connVar = ("Boolean val: " + strconv.FormatBool(con))
	m.Data["Connection"] = connVar
	connVar = ("Int val: " + strconv.FormatInt(valInt, 16))
	m.Data["ConnectionInt"] = connVar
	m.TplNames = "connected.tpl"

}
