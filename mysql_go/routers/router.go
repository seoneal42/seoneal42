package routers

import (
	"github.com/astaxie/beego"
	"github.com/seoneal42/mysql_go/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/connected", &controllers.MysqlController{})
}
